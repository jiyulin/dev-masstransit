﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dev.MT.Commands;
using Dev.MT.Domain.Models;
using Dev.MT.Repository;
using Dev.MT.ViewModels;
using MassTransit;
using AutoMapper.QueryableExtensions;

namespace Dev.MT.ApplicationServices
{
    public class LeaveService : ServiceBase
    {
        public LeaveService(IBus bus, DevMtDbContext dbContext)
            : base(bus, dbContext)
        {

        }

        public CreateLeaveViewModel CreateCreateLeaveViewModel()
        {
            return new CreateLeaveViewModel
            {
                JobGrade = 10,
                Start = DateTime.Now,
                End = DateTime.Now,
                Hours = 8,
            };
        }

        public async Task<IEnumerable<LeaveDetailsViewModel>> CreateLeaveOverviewViewModelAsync()
        {
            var leaves = await (from item in DbContext.Leaves
                                orderby item.Id descending
                                select item).Take(5).ToListAsync();

            return leaves.AsQueryable().ProjectTo<LeaveDetailsViewModel>();
        }

        public async Task<LeaveDetailsViewModel> CreateLeaveDetailsViewModelAsync(int leaveId)
        {
            var leave = await DbContext.Leaves.Include(x => x.Activities)
                                .Where(x => x.Id == leaveId)
                                .SingleAsync();

            if (leave == null)
                throw new ArgumentException($"Cound not find leave with the id [{leaveId}] that was provided.");

            return AutoMapper.Mapper.Map<LeaveDetailsViewModel>(leave);
        }

        public async Task<int> CreateLeaveAsync(CreateLeaveViewModel vm)
        {
            var command = AutoMapper.Mapper.Map<CreateLeave>(vm);

            var result = await base.RequestAsync<CreateLeave, CreateLeaveResponse>(command);

            return result.LeaveId;
        }

        public async Task SubmitLeaveAsync(int leaveId)
        {
            var command = new SubmitLeave(leaveId);
            await base.RequestAsync<SubmitLeave, SubmitLeaveResponse>(command);
        }

        public async Task CancelLeaveAsync(int leaveId)
        {
            var command = new CancelLeave(leaveId);
            await base.RequestAsync<CancelLeave, CancelLeaveResponse>(command);
        }

        public async Task ApproveByLeaderAsync(int leaveId)
        {
            var command = new ApproveLeaveByLeader(leaveId);
            await base.RequestAsync<ApproveLeaveByLeader, ApproveLeaveByLeaderResponse>(command);
        }

        public async Task DeclineByLeaderAsync(int leaveId)
        {
            var command = new DeclineLeaveByLeader(leaveId);
            await base.RequestAsync<DeclineLeaveByLeader, DeclineLeaveByLeaderResponse>(command);
        }

        public async Task ApproveByManagerAsync(int leaveId)
        {
            var command = new ApproveLeaveByManager(leaveId);
            await base.RequestAsync<ApproveLeaveByManager, ApproveLeaveByManagerResponse>(command);
        }

        public async Task DeclineByManagerAsync(int leaveId)
        {
            var command = new DeclineLeaveByManager(leaveId);
            await base.RequestAsync<DeclineLeaveByManager, DeclineLeaveByManagerResponse>(command);
        }
    }
}
