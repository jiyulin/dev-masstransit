﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dev.MT.Repository;
using MassTransit;

namespace Dev.MT.ApplicationServices
{
    public abstract class ServiceBase
    {
        protected IBus Bus { get; private set; }
        protected DevMtDbContext DbContext { get; private set; }

        protected ServiceBase(IBus bus, DevMtDbContext dbContext)
        {
            Bus = bus;
            DbContext = dbContext;
        }

        protected async Task<TResponse> RequestAsync<TCommand, TResponse>(TCommand command)
            where TCommand : class
            where TResponse : class
        {
            if (Bus == null)
                throw new NullReferenceException("Bus");

            if (command == null)
                throw new ArgumentNullException(nameof(command));

            // Create a new unique correlation id for MassTransit to match the request message
            // to the response message.
            var correlationId = NewId.NextGuid();
            Task<TResponse> response = null;

            await Bus.Request(new Uri("loopback://localhost/input_queue"), command, x =>
            {
                x.CorrelationId = correlationId;
                response = x.Handle<TResponse>(async context => Console.WriteLine("Response received"));
            });

            await response;

            return response.Result;
        }
    }
}
