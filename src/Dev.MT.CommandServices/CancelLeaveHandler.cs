﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MassTransit;
using Dev.MT.Commands;
using Dev.MT.Domain.Models;
using Dev.MT.Events;
using Dev.MT.Repository;

namespace Dev.MT.CommandServices
{
    public class CancelLeaveHandler : IConsumer<CancelLeave>
    {
        protected DevMtDbContext DbContext { get; private set; }

        public CancelLeaveHandler(DevMtDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public async Task Consume(ConsumeContext<CancelLeave> context)
        {
            var command = context.Message;

            var leave = await DbContext.Leaves.FindAsync(command.LeaveId);

            if (leave == null)
                throw new ArgumentException($"Cound not find leave with the id [{command.LeaveId}] that was provided.");

            leave.Cancel();

            await DbContext.SaveChangesAsync();

            var @event = new LeaveCancelled(leave.Id);
            await context.Publish(@event);

            await context.RespondAsync(new CancelLeaveResponse(leave.Id));
        }
    }
}
