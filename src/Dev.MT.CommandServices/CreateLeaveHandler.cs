﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MassTransit;
using Dev.MT.Commands;
using Dev.MT.Domain.Models;
using Dev.MT.Repository;

namespace Dev.MT.CommandServices
{
    public class CreateLeaveHandler : IConsumer<CreateLeave>
    {
        protected DevMtDbContext DbContext { get; private set; }

        public CreateLeaveHandler(DevMtDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public async Task Consume(ConsumeContext<CreateLeave> context)
        {
            var command = context.Message;

            var leave = Leave.Create(command.JobGrade, command.Start, command.End, command.Hours, command.Note);

            DbContext.Leaves.Add(leave);

            await DbContext.SaveChangesAsync();

            await context.RespondAsync(new CreateLeaveResponse(leave.Id));
        }
    }
}
