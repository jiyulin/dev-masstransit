﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MassTransit;
using Dev.MT.Commands;
using Dev.MT.Domain.Models;
using Dev.MT.Events;
using Dev.MT.Repository;

namespace Dev.MT.CommandServices
{
    public class LeaveActivityHandler :
        IConsumer<LeaveSubmitted>,
        IConsumer<LeaveCancelled>,
        IConsumer<LeaveApprovedByLeader>,
        IConsumer<LeaveDeclinedByLeader>,
        IConsumer<LeaveApprovedByManager>,
        IConsumer<LeaveDeclinedByManager>
    {
        protected DevMtDbContext DbContext { get; private set; }

        public LeaveActivityHandler(DevMtDbContext dbContext)
        {
            DbContext = dbContext;
        }

        protected async Task AddActivity(int leaveId, string desc)
        {
            var leave = await DbContext.Leaves.FindAsync(leaveId);

            if (leave == null)
                throw new ArgumentException($"Cound not find leave with the id [{leaveId}] that was provided.");

            leave.AddActivity(desc);

            await DbContext.SaveChangesAsync();
        }

        public async Task Consume(ConsumeContext<LeaveSubmitted> context)
        {
            var command = context.Message;

            await AddActivity(command.LeaveId, "Applicant submitted the leave.");
        }

        public async Task Consume(ConsumeContext<LeaveCancelled> context)
        {
            var command = context.Message;

            await AddActivity(command.LeaveId, "The leave was cancelled.");
        }

        public async Task Consume(ConsumeContext<LeaveApprovedByLeader> context)
        {
            var command = context.Message;

            await AddActivity(command.LeaveId, "Approved by Leader.");
        }

        public async Task Consume(ConsumeContext<LeaveDeclinedByLeader> context)
        {
            var command = context.Message;

            await AddActivity(command.LeaveId, "Declined by Leader.");
        }

        public async Task Consume(ConsumeContext<LeaveApprovedByManager> context)
        {
            var command = context.Message;

            await AddActivity(command.LeaveId, "Approved by Manager.");
        }

        public async Task Consume(ConsumeContext<LeaveDeclinedByManager> context)
        {
            var command = context.Message;

            await AddActivity(command.LeaveId, "Declined by Manager.");
        }
    }
}
