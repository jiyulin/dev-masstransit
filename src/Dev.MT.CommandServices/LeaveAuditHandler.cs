﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MassTransit;
using Dev.MT.Commands;
using Dev.MT.Domain.Models;
using Dev.MT.Events;
using Dev.MT.Repository;

namespace Dev.MT.CommandServices
{
    public class LeaveAuditHandler :
        IConsumer<LeaveApproved>,
        IConsumer<LeaveDeclined>
    {
        protected DevMtDbContext DbContext { get; private set; }

        public LeaveAuditHandler(DevMtDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public async Task Consume(ConsumeContext<LeaveApproved> context)
        {
            var command = context.Message;

            var leave = await DbContext.Leaves.FindAsync(command.LeaveId);

            if (leave == null)
                throw new ArgumentException($"Cound not find leave with the id [{command.LeaveId}] that was provided.");

            leave.Approve();

            await DbContext.SaveChangesAsync();
        }

        public async Task Consume(ConsumeContext<LeaveDeclined> context)
        {
            var command = context.Message;

            var leave = await DbContext.Leaves.FindAsync(command.LeaveId);

            if (leave == null)
                throw new ArgumentException($"Cound not find leave with the id [{command.LeaveId}] that was provided.");

            leave.Decline();

            await DbContext.SaveChangesAsync();
        }
    }
}
