﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MassTransit;
using Dev.MT.Commands;
using Dev.MT.Domain.Models;
using Dev.MT.Events;
using Dev.MT.Repository;
using MassTransit.Logging;

namespace Dev.MT.CommandServices
{
    public class NotifyApplicantLeaveApprovedHandler : IConsumer<NotifyApplicantLeaveApproved>
    {
        protected log4net.ILog Log { get; private set; }
        protected DevMtDbContext DbContext { get; private set; }

        public NotifyApplicantLeaveApprovedHandler(log4net.ILog log, DevMtDbContext dbContext)
        {
            Log = log;
            DbContext = dbContext;
        }

        public async Task Consume(ConsumeContext<NotifyApplicantLeaveApproved> context)
        {
            var command = context.Message;

            var leave = await DbContext.Leaves.FindAsync(command.LeaveId);

            if (leave == null)
                throw new ArgumentException($"Cound not find leave with the id [{command.LeaveId}] that was provided.");

            Log.Info($"Approved by Leader [LeaveId = {leave.Id}]");
        }
    }
}
