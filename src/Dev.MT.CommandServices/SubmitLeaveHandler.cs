﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MassTransit;
using Dev.MT.Commands;
using Dev.MT.Domain.Models;
using Dev.MT.Events;
using Dev.MT.Repository;

namespace Dev.MT.CommandServices
{
    public class SubmitLeaveHandler : IConsumer<SubmitLeave>
    {
        protected DevMtDbContext DbContext { get; private set; }

        public SubmitLeaveHandler(DevMtDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public async Task Consume(ConsumeContext<SubmitLeave> context)
        {
            var command = context.Message;

            var leave = await DbContext.Leaves.FindAsync(command.LeaveId);

            if (leave == null)
                throw new ArgumentException($"Cound not find leave with the id [{command.LeaveId}] that was provided.");

            leave.Submit();

            await DbContext.SaveChangesAsync();

            var @event = new LeaveSubmitted(leave.Id);
            await context.Publish(@event);

            await context.RespondAsync(new SubmitLeaveResponse(leave.Id));
        }
    }
}
