﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dev.MT.Commands
{
    public class CreateLeave
    {
        public CreateLeave()
        {
        }

        public int JobGrade { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int Hours { get; set; }
        public string Note { get; set; }
    }

    public class CreateLeaveResponse
    {
        public CreateLeaveResponse(int leaveId)
        {
            if(leaveId==default(int)) throw new ArgumentOutOfRangeException(nameof(leaveId));

            LeaveId = leaveId;
        }

        public int LeaveId { get; private set; }
    }
}
