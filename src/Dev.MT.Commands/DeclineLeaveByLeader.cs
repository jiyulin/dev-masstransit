﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dev.MT.Commands
{
    public class DeclineLeaveByLeader
    {
        public DeclineLeaveByLeader(int leaveId)
        {
            if (leaveId == default(int)) throw new ArgumentOutOfRangeException(nameof(leaveId));

            LeaveId = leaveId;
        }

        public int LeaveId { get; private set; }
    }

    public class DeclineLeaveByLeaderResponse
    {
        public DeclineLeaveByLeaderResponse(int leaveId)
        {
            if (leaveId == default(int)) throw new ArgumentOutOfRangeException(nameof(leaveId));

            LeaveId = leaveId;
        }

        public int LeaveId { get; private set; }
    }
}
