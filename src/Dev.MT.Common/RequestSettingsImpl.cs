﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automatonymous;

namespace Dev.MT.Common
{
    public class RequestSettingsImpl : RequestSettings
    {
        public Uri ServiceAddress { get; set; }
        public Uri SchedulingServiceAddress { get; set; }
        public TimeSpan Timeout { get; set; }
    }
}
