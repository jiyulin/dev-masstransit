﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dev.MT.Domain
{
    public abstract class EntityBase : EntityBase<int>
    {
    }

    public abstract class EntityBase<TKey>
    {
        public TKey Id { get; set; }
    }
}
