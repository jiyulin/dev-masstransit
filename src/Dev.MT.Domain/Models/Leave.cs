﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dev.MT.Domain.Models
{
    public class Leave : EntityBase
    {
        public int JobGrade { get; protected set; }
        public DateTime Start { get; protected set; }
        public DateTime End { get; protected set; }
        public int Hours { get; protected set; }
        public string Note { get; protected set; }
        public string State { get; protected set; }
        public ICollection<LeaveActivity> Activities { get; private set; }

        public Leave()
        {
            Activities = new HashSet<LeaveActivity>();
        }

        public static Leave Create(int jobGrade, DateTime start, DateTime end, int hours, string note)
        {
            return new Leave()
            {
                JobGrade = jobGrade,
                Start = start,
                End = end,
                Hours = hours,
                Note = note,
                State = LeaveState.Draft,
            };
        }

        public void AddActivity(string desc)
        {
            var activity = LeaveActivity.Create(desc);
            this.Activities.Add(activity);
        }

        public void Submit()
        {
            this.State = LeaveState.Submitted;
        }

        public void Cancel()
        {
            this.State = LeaveState.Cancelled;
        }

        public void Approve()
        {
            this.State = LeaveState.Approved;
        }

        public void Decline()
        {
            this.State = LeaveState.Declined;
        }
    }

    public struct LeaveState
    {
        public const string Draft = "DRAFT";
        public const string Submitted = "SUBMITTED";
        public const string Approved = "APPROVED";
        public const string Declined = "DECLINED";
        public const string Cancelled = "CANCELLED";
    }
}
