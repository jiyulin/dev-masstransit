﻿using System;

namespace Dev.MT.Domain.Models
{
    public class LeaveActivity : EntityBase
    {
        public virtual Leave Leave { get; protected set; }
        public DateTime Date { get; protected set; }
        public string Description { get; protected set; }

        public static LeaveActivity Create(string desc)
        {
            return new LeaveActivity
            {
                Date = DateTime.Now,
                Description = desc,
            };
        }
    }
}