﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dev.MT.Events
{
    public class LeaveDeclinedByLeader
    {
        public LeaveDeclinedByLeader(int leaveId, int jobGrade)
        {
            if (leaveId == default(int)) throw new ArgumentOutOfRangeException(nameof(leaveId));
            if (jobGrade == default(int)) throw new ArgumentOutOfRangeException(nameof(jobGrade));

            LeaveId = leaveId;
            JobGrade = jobGrade;
        }

        public int LeaveId { get; private set; }
        public int JobGrade { get; private set; }
    }
}
