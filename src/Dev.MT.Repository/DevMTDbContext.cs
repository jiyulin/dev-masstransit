﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Dev.MT.Domain.Models;
using Dev.MT.Repository.Migrations;

namespace Dev.MT.Repository
{
    public class DevMtDbContext : DbContext
    {
        static DevMtDbContext()
        {
            // Set the db initializer for the context type only once when the type is initialized.
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DevMtDbContext, Configuration>());
        }

        public DevMtDbContext()
            : base("name=DevMT")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("dev");

            modelBuilder.Configurations.AddFromAssembly(Assembly.GetExecutingAssembly());

            base.OnModelCreating(modelBuilder);
        }

        #region DbSets

        public virtual DbSet<Leave> Leaves { get; set; }

        #endregion
    }
}
