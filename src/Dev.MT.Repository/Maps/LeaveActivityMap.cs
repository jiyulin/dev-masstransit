﻿using System.Data.Entity.ModelConfiguration;
using Dev.MT.Domain.Models;

namespace Dev.MT.Repository.Maps
{
    public class LeaveActivityMap : EntityTypeConfiguration<LeaveActivity>
    {
        public LeaveActivityMap()
        {
            Property(m => m.Description).HasMaxLength(255);
        }
    }
}