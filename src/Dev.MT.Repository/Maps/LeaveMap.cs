﻿using System.Data.Entity.ModelConfiguration;
using Dev.MT.Domain.Models;

namespace Dev.MT.Repository.Maps
{
    public class LeaveMap : EntityTypeConfiguration<Leave>
    {
        public LeaveMap()
        {
            Property(m => m.Note).HasMaxLength(255);
            Property(m => m.State).HasMaxLength(50);

            HasMany(m => m.Activities)
                .WithRequired(m => m.Leave)
                .Map(c =>
                {
                    c.MapKey("LeaveId");
                })
                .WillCascadeOnDelete();
        }
    }
}