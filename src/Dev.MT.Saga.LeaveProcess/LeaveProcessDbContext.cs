﻿using System.Data.Entity;
using Dev.MT.Saga.LeaveProcess.Migrations;
using MassTransit.EntityFrameworkIntegration;

namespace Dev.MT.Saga.LeaveProcess
{
    public class LeaveProcessDbContext
        : SagaDbContext<LeaveProcessState, LeaveProcessStateMap>
    {
        static LeaveProcessDbContext()
        {
            // Set the db initializer for the context type only once when the type is initialized.
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<LeaveProcessDbContext, Configuration>());
        }

        public LeaveProcessDbContext()
            : base("name=DevMT")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("state");

            base.OnModelCreating(modelBuilder);
        }
    }
}