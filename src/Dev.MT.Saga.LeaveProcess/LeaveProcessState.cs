﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automatonymous;

namespace Dev.MT.Saga.LeaveProcess
{
    public class LeaveProcessState: SagaStateMachineInstance
    {
        public Guid CorrelationId { get; set; }
        public string CurrentState { get; set; }
        public int? LeaveId { get; set; }
    }
}
