﻿using MassTransit.EntityFrameworkIntegration;

namespace Dev.MT.Saga.LeaveProcess
{
    public class LeaveProcessStateMap : SagaClassMapping<LeaveProcessState>
    {
        public LeaveProcessStateMap()
        {
            Property(x => x.CurrentState).HasMaxLength(64);
            Property(x => x.CurrentState);
            Property(x => x.LeaveId);
        }
    }
}