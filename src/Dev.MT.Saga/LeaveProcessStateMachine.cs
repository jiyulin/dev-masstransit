﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automatonymous;
using Dev.MT.Commands;
using Dev.MT.Events;
using Dev.MT.Saga.LeaveProcess;
using MassTransit;

namespace Dev.MT.Saga
{
    public class LeaveProcessStateMachine
        : MassTransitStateMachine<LeaveProcessState>
    {
        public LeaveProcessStateMachine(RequestSettings settings)
        {
            InstanceState(x => x.CurrentState);

            Event(() => LeaveSubmitted,
                x => x.CorrelateBy(p => p.LeaveId, p => (int?)p.Message.LeaveId).SelectId(context => NewId.NextGuid()));
            Event(() => LeaveApprovedByLeader, x => x.CorrelateBy((state, context) => state.LeaveId == context.Message.LeaveId));
            Event(() => LeaveDeclinedByLeader, x => x.CorrelateBy((state, context) => state.LeaveId == context.Message.LeaveId));
            Event(() => LeaveApprovedByManager, x => x.CorrelateBy((state, context) => state.LeaveId == context.Message.LeaveId));
            Event(() => LeaveDeclinedByManager, x => x.CorrelateBy((state, context) => state.LeaveId == context.Message.LeaveId));
            Event(() => LeaveCancelled, x => x.CorrelateBy((state, context) => state.LeaveId == context.Message.LeaveId));

            Initially(
                When(LeaveSubmitted)
                    .Then(context =>
                    {
                        context.Instance.LeaveId = context.Data.LeaveId;
                    })
                    .Publish(context => new NotifyLeaderAuditLeave(context.Data.LeaveId))
                    .TransitionTo(WaitingLeaderAudit));

            During(WaitingLeaderAudit,
                When(LeaveApprovedByLeader, context => context.Data.JobGrade < 10)
                    .Publish(context => new LeaveApproved(context.Data.LeaveId))
                    .Publish(context => new NotifyApplicantLeaveApproved(context.Data.LeaveId))
                    .Finalize(),
                When(LeaveDeclinedByLeader)
                    .Publish(context => new LeaveDeclined(context.Data.LeaveId))
                    .Publish(context => new NotifyApplicantLeaveDeclined(context.Data.LeaveId))
                    .Finalize(),
                When(LeaveApprovedByLeader, context => context.Data.JobGrade >= 10)
                    .Publish(context => new NotifyManagerAuditLeave(context.Data.LeaveId))
                    .TransitionTo(WaitingManagerAudit));

            During(WaitingManagerAudit,
                When(LeaveApprovedByManager)
                    .Publish(context => new LeaveApproved(context.Data.LeaveId))
                    .Publish(context => new NotifyApplicantLeaveApproved(context.Data.LeaveId))
                    .Finalize(),
                When(LeaveDeclinedByManager)
                    .Publish(context => new LeaveDeclined(context.Data.LeaveId))
                    .Publish(context => new NotifyApplicantLeaveDeclined(context.Data.LeaveId))
                    .Finalize());

            During(WaitingLeaderAudit, WaitingManagerAudit,
                When(LeaveCancelled)
                    .Finalize());
        }

        public State WaitingLeaderAudit { get; private set; }
        public State WaitingManagerAudit { get; private set; }

        public Event<LeaveSubmitted> LeaveSubmitted { get; private set; }
        public Event<LeaveApprovedByLeader> LeaveApprovedByLeader { get; private set; }
        public Event<LeaveDeclinedByLeader> LeaveDeclinedByLeader { get; private set; }
        public Event<LeaveApprovedByManager> LeaveApprovedByManager { get; private set; }
        public Event<LeaveDeclinedByManager> LeaveDeclinedByManager { get; private set; }
        public Event<LeaveCancelled> LeaveCancelled { get; private set; }
    }
}
