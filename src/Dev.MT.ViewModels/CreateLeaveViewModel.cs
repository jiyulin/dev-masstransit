﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dev.MT.ViewModels
{
    public class CreateLeaveViewModel
    {
        [Required]
        [DisplayName("Job Grade")]
        [Range(1, 16)]
        public int JobGrade { get; set; }
        [Required]
        [DisplayName("From")]
        public DateTime Start { get; set; }
        [Required]
        [DisplayName("To")]
        public DateTime End { get; set; }
        [Required]
        [Range(0, int.MaxValue)]
        public int Hours { get; set; }
        [Required]
        public string Note { get; set; }
    }
}
