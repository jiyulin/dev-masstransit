﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Dev.MT.Commands;
using Dev.MT.ViewModels;
using Dev.MT.Domain.Models;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Dev.MT.Web.App_Start.AutoMapperConfig), "Start")]
namespace Dev.MT.Web.App_Start
{
    public static class AutoMapperConfig
    {
        public static void Start()
        {
            Mapper.CreateMap<CreateLeaveViewModel, CreateLeave>();
            Mapper.CreateMap<Leave, LeaveDetailsViewModel>()
                .ForMember(d => d.LeaveId, o => o.MapFrom(s => s.Id));
            Mapper.CreateMap<LeaveActivity, LeaveActivityDetailsViewModel>();

            Mapper.AssertConfigurationIsValid();
        }
    }
}